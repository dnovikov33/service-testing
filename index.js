function testService(events) {
    let test = {}
    let result = true
    events.forEach(event => {
        const user = event[0]
        const type = event[1]
        switch (type) {
            case 'in': {
                if (test.hasOwnProperty(user)) {
                    result = false
                }
                test[user] = true
                break
            }
            case 'out': {
                if (!test.hasOwnProperty(user)) {
                    result = false
                }
                delete(test[user])
                break
            }
            default: {
                result = false
            }
        }
    })
    if (result) {
        return Object.keys(test).length === 0
    }
    return result
}

module.exports = testService
